#!/bin/sh
mouse_device=$(xinput --list --id-only "Primax Kensington Eagle Trackball")
mouse_props=$(xinput --list-props $mouse_device | grep Middle | grep -oP '(?<=\()[[:digit:]]+(?=\))')
for prop in $mouse_props; do
    xinput --set-prop $mouse_device $prop 1
    if [ $? -eq 0 ]; then
        echo "Middle button $prop is activated! :D"
        break
    fi
done

